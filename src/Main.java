class EmptyArray extends Exception{
    public EmptyArray(String message) {
        super(message);
    }
}

class Negatives extends Exception {
    public Negatives(String message) {
        super(message);
    }
}


public class Main {
    public static void main(String[] args) {
        int[] nums = new int[] {-1, 2, 3, 5};
        Float result;

        try {
            whichIsNegative(nums);
            result = avg(nums);
            if (result != null) {
                System.out.println(result);
            }
        } catch(EmptyArray e) {
            System.out.println(e.getMessage());
        } catch(Negatives e) {
            System.out.println(e.getMessage());
        }
    }


    public static void whichIsNegative(int[] nums) throws Negatives {
        boolean hasNegatives = false;

        for (int n : nums) {
            if (n < 0) {
                hasNegatives = true;
                break;
            }
        }

        if (hasNegatives) {
            StringBuilder resultString = new StringBuilder();

            int iteration = 0;
            String first = "st";
            String second = "nd";
            String third = "rd";
            String more = "th";

            for (int n : nums) {
                if (n < 0) {
                    if (iteration == 0) {
                        resultString.append("The ").append(1 + iteration).append(first).append(" number ").append(n).append(" in your array is invalid\n");
                    } else if (iteration == 1) {
                        resultString.append("The ").append(1 + iteration).append(second).append(" number ").append(n).append(" in your array is invalid\n");
                    } else if (iteration == 2) {
                        resultString.append("The ").append(1 + iteration).append(third).append(" number ").append(n).append(" in your array is invalid\n");
                    } else {
                        resultString.append("The ").append(1 + iteration).append(more).append(" number ").append(n).append(" in your array is invalid\n");
                    }
                }
                iteration++;
            }
            throw new Negatives(resultString.toString());
        }
    }


    public static float avg(int[] nums) throws EmptyArray {
        int sum = 0;
        if (nums == null || nums.length == 0)
            throw new EmptyArray("The Array is empty :(");

        for (int n : nums) {
            sum += n;
        }

        return sum / (float) nums.length;
    }
}