Let's examine the exception example from "part" 5 of the lecture material on page 19.

class EmptyArray extends Exception {}

float avg(int[] nums) throws EmptyArray { // 1
    int sum = 0;
    if (nums == null || nums.length == 0)
        throw new EmptyArray(); // 2
    for(int n: nums) sum += n;
    return sum / nums.length;
}
void main() {
    int[] nums = new int[] { 1, 2 };
    Float result;
    try { result = avg(nums); } // 3
    catch(EmptyArray e) {} // 3
}
 

Task: What is the value of the variable result at the end of the program when the variable nums refers to the array [1, 2] accepted by the routine avg? What about when the array is empty? Why?

Modify the routine avg so that it only accepts arrays composed of non-negative numbers (x >= 0). If the array contains negative numbers, the routine should inform the client of all the indices where there are negative numbers in the array. The routine avg only informs the client, it does not print anything itself. Using this information, the client could do various things, but here wants to print a message "The X-th number Y in your array is invalid" for each case of a negative number. Here X would be the array index (starting from value 1) and Y the value at that index.

Example:

Input:

[ 1, -2, -3, 4 ]
Output:

The 2nd number -2 in your array is invalid
The 3rd number -3 in your array is invalid
 

Example 2:

Input

[ 1, 2, 3 ]
Output

2.0